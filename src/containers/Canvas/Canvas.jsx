import React, { Component } from 'react';
import { Human } from '../../graphics';
import { Key } from '../../constants';

const SPEED = 5;
const BOOST_SPEED = 10;
const FPS = 60;

export class Canvas extends Component {
    constructor(props) {
        super(props);

        this.state = {
            options: {
                width: window.visualViewport.width,
                height: window.visualViewport.height,
            },
            figure: {
                x: 100,
                y: 100,
                width: 50,
                height: 50,
            },
        };

        this.KeyState = {
            up: false,
            right: false,
            down: false,
            left: false,
            shift: false,
        };

        this.gameLoop = setInterval(this.updateCanvas, 1000 / FPS);
    }

    ctx = null;

    componentDidMount() {
        const { x, y } = this.state.figure;
        console.log('mount');

        this.ctx = this.refs.canvas.getContext('2d');
        this.hero = new Human(this.ctx, x, y);
        this.updateCanvas();

        window.addEventListener('keydown', this.handleKeyDown, true);
        window.addEventListener('keyup', this.handleKeyUp, true);
    }

    componentWillUnmount() {
        window.removeEventListener('keydown', this.handleKeyDown);
        window.removeEventListener('keyup', this.handleKeyUp);
        clearInterval(this.gameLoop);
    }

    handleKeyDown = event => {
        switch (event.keyCode) {
            case Key.UP_ARROW:
                this.KeyState.up = true;
                break;
            case Key.RIGHT_ARROW:
                this.KeyState.right = true;
                break;
            case Key.DOWN_ARROW:
                this.KeyState.down = true;
                break;
            case Key.LEFT_ARROW:
                this.KeyState.left = true;
                break;
            case Key.SHIFT:
                this.KeyState.shift = true;
                break;
            default:
                break;
        }
    }

    handleKeyUp = event => {
        switch (event.keyCode) {
            case Key.UP_ARROW:
                this.KeyState.up = false;
                break;
            case Key.RIGHT_ARROW:
                this.KeyState.right = false;
                break;
            case Key.DOWN_ARROW:
                this.KeyState.down = false;
                break;
            case Key.LEFT_ARROW:
                this.KeyState.left = false;
                break;
            case Key.SHIFT:
                this.KeyState.shift = false;
                break;
            default:
                break;
        }
    }

    updateCoords = () => {
        const { x, y } = this.state.figure;
        let newX = x;
        let newY = y;

        if (this.KeyState.up) {
            newY = y - (this.KeyState.shift ? BOOST_SPEED : SPEED);
        } else if (this.KeyState.down) {
            newY = y + (this.KeyState.shift ? BOOST_SPEED : SPEED);
        }

        if (this.KeyState.right) {
            newX = x + (this.KeyState.shift ? BOOST_SPEED : SPEED);
        } else if (this.KeyState.left) {
            newX = x - (this.KeyState.shift ? BOOST_SPEED : SPEED);
        }

        if (newX !== x || newY !== y) {
            this.setState(prevState => ({
                figure: {
                    ...prevState.figure,
                    x: newX,
                    y: newY,
                },
            }));
        }
    }

    updateCanvas = () => {
        console.log('loop');
        this.updateCoords();
        const { options, figure: { x, y } } = this.state;

        this.ctx.clearRect(0, 0, options.width, options.height);

        if (this.KeyState.right) {
            this.hero.direction = 'right';
        } else if (this.KeyState.left) {
            this.hero.direction = 'left';
        }
        this.hero.x = x;
        this.hero.y = y;
        this.hero.redraw();
    }

    render() {
        const { options: { width, height } } = this.state;

        return (
            <canvas
                ref="canvas"
                width={width}
                height={height}
            />
        );
    }
}
