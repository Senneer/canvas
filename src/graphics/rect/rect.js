export const rect = props => {
    const { ctx, x, y, width, height } = props;
    ctx.fillRect(x, y, width, height);
};
