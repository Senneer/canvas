import {
    head,
    torso,
    legs,
    hair,
    leftArm,
    rightArm,
    headReverse,
    torsoReverse,
    legsReverse,
    hairReverse,
    leftArmReverse,
    rightArmReverse
} from './images';
import { loadImg } from '../../helpers/images';

const Direction = {
    RIGHT: 'right',
    LEFT: 'left',
};

const Breath = {
    IN: 'in',
    OUT: 'out',
};

export class Human {
    constructor(ctx, initX, initY) {
        this.ctx = ctx;
        this.x = initX;
        this.y = initY;
        this.direction = Direction.LEFT;

        this.currentEyeHeight = this._MAX_EYE_HEIGHT;
        this.eyeOpenTime = 0;

        this.breathAmt = 0;
        this.breathDirection = Breath.IN;

        this.bodyPart = {
            head: loadImg(head),
            torso: loadImg(torso),
            legs: loadImg(legs),
            hair: loadImg(hair),
            leftArm: loadImg(leftArm),
            rightArm: loadImg(rightArm),
        };
        this.reversedBodyPart = {
            head: loadImg(headReverse),
            torso: loadImg(torsoReverse),
            legs: loadImg(legsReverse),
            hair: loadImg(hairReverse),
            leftArm: loadImg(leftArmReverse),
            rightArm: loadImg(rightArmReverse),
        };

        this.blinkTimer = setInterval(this._updateBlink, this._BLINK_UPDATE_TIME);
        this.breathInterval = setInterval(this._updateBreath, 1000 / 60);
    }

    _MAX_EYE_HEIGHT = 14;
    _TIME_BTW_BLINKS = 4000;
    _BLINK_UPDATE_TIME = 200;

    _BREATH_INC = 0.1;
    _MAX_BREATH = 2;

    redraw() {
        this._drawShadow();
        this._drawBody();
        this._drawEyes();
    }

    _drawBody() {
        const { ctx, bodyPart, reversedBodyPart, x, y, direction } = this;

        if (direction === Direction.RIGHT) {
            const { leftArm, torso, legs, rightArm, head, hair } = bodyPart;

            ctx.drawImage(leftArm, x + 50, y + 90 - this.breathAmt);
            ctx.drawImage(torso, x + 13, y + 70);
            ctx.drawImage(legs, x + 13, y + 120);
            ctx.drawImage(rightArm, x, y + 85 - this.breathAmt);
            ctx.drawImage(head, x, y - this.breathAmt);
            ctx.drawImage(hair, x - 27, y - 13 - this.breathAmt);
        } else {
            const { leftArm, torso, legs, rightArm, head, hair } = reversedBodyPart;

            ctx.drawImage(leftArm, x - 20, y + 90 - this.breathAmt);
            ctx.drawImage(torso, x + 13, y + 70);
            ctx.drawImage(legs, x - 13, y + 120);
            ctx.drawImage(rightArm, x + 50, y + 85 - this.breathAmt);
            ctx.drawImage(head, x, y - this.breathAmt);
            ctx.drawImage(hair, x - 3, y - 13 - this.breathAmt);
        }
    };

    _drawEyes() {
        const { ctx, x, y, direction, currentEyeHeight } = this;

        if (direction === Direction.RIGHT) {
            drawEllipse(ctx, x + 58, y + 55 - this.breathAmt, 8, currentEyeHeight); // Left Eye
            drawEllipse(ctx, x + 69, y + 55 - this.breathAmt, 8, currentEyeHeight); // Right Eye
        } else {
            drawEllipse(ctx, x + 22, y + 55 - this.breathAmt, 8, currentEyeHeight); // Left Eye
            drawEllipse(ctx, x + 33, y + 55 - this.breathAmt, 8, currentEyeHeight); // Right Eye
        }
    };

    _drawShadow() {
        const { ctx, x, y, direction } = this;

        if (direction === Direction.RIGHT) {
            drawEllipse(ctx, x + 55, y + 151, 160 - this.breathAmt, 6);
        } else {
            drawEllipse(ctx, x + 32, y + 151, 160 - this.breathAmt, 6);
        }
    };

    _updateBlink = () => {
        const { _BLINK_UPDATE_TIME, _TIME_BTW_BLINKS } = this;

        this.eyeOpenTime += _BLINK_UPDATE_TIME;

        if (this.eyeOpenTime >= _TIME_BTW_BLINKS) {
            this._blink();
        }
    };

    _blink = () => {
        const { _MAX_EYE_HEIGHT } = this;

        this.currentEyeHeight -= 1;

        if (this.currentEyeHeight <= 0) {
            this.eyeOpenTime = 0;
            this.currentEyeHeight = _MAX_EYE_HEIGHT;
        } else {
            setTimeout(this._blink, 10);
        }
    };

    _updateBreath = () => {
        const { _BREATH_INC, _MAX_BREATH } = this;

        if (this.breathDirection === Breath.IN) {
            this.breathAmt -= _BREATH_INC;

            if (this.breathAmt < (-_MAX_BREATH)) {
                this.breathDirection = Breath.OUT;
            }
        } else {
            this.breathAmt += _BREATH_INC;

            if (this.breathAmt > _MAX_BREATH) {
                this.breathDirection = Breath.IN;
            }
        }
    };
};

function drawEllipse(context, centerX, centerY, width, height) {
    context.beginPath();

    context.moveTo(centerX, centerY - height / 2);

    context.bezierCurveTo(
        centerX + width / 2, centerY - height / 2,
        centerX + width / 2, centerY + height / 2,
        centerX, centerY + height / 2);

    context.bezierCurveTo(
        centerX - width / 2, centerY + height / 2,
        centerX - width / 2, centerY - height / 2,
        centerX, centerY - height / 2);

    context.fillStyle = "black";
    context.fill();
    context.closePath();
};
