import head from '../../assets/img/head.png';
import torso from '../../assets/img/torso.png';
import legs from '../../assets/img/legs.png';
import hair from '../../assets/img/hair.png';
import leftArm from '../../assets/img/leftArm.png';
import rightArm from '../../assets/img/rightArm.png';

import headReverse from '../../assets/img/head-reverse.png';
import torsoReverse from '../../assets/img/torso-reverse.png';
import legsReverse from '../../assets/img/legs-reverse.png';
import hairReverse from '../../assets/img/hair-reverse.png';
import leftArmReverse from '../../assets/img/leftArm-reverse.png';
import rightArmReverse from '../../assets/img/rightArm-reverse.png';

export {
    head,
    torso,
    legs,
    hair,
    leftArm,
    rightArm,
    headReverse,
    torsoReverse,
    legsReverse,
    hairReverse,
    leftArmReverse,
    rightArmReverse
};
