import React, { Component } from 'react';
import { Canvas } from './containers';
import './App.css';

export default class App extends Component {
    render() {
        return (
            <Canvas />
        );
    }
}
