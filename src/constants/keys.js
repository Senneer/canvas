export const Key = {
    UP_ARROW: 38,
    RIGHT_ARROW: 39,
    DOWN_ARROW: 40,
    LEFT_ARROW: 37,
    SHIFT: 16,
};
